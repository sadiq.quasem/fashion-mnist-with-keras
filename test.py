import tensorflow as tf
from tensorflow import keras
import random
import numpy as np


def loadModel(jsonStr, weightStr):
    json_file = open(jsonStr, 'r')
    loaded_nnet = json_file.read()
    json_file.close()

    serve_model = tf.keras.models.model_from_json(loaded_nnet)
    serve_model.load_weights(weightStr)

    serve_model.compile(optimizer=tf.train.AdamOptimizer(),
                        loss='categorical_crossentropy',
                        metrics=['accuracy'])
    return serve_model


def inference(input):
    input = input.reshape(1, 28, 28, 1)
    inferenceVal = model.predict(input)
    retVal = np.argmax(inferenceVal)
    return retVal


model = loadModel('model.json', 'model.h5')

(train_images, train_labels), (test_images,
                               test_labels) = keras.datasets.fashion_mnist.load_data()

test_images = test_images / 255.0

index = []
for i in range(5):
    index.append(random.randint(0, 9999))
    inferenceString = inference(test_images[index[i]])
    print("Predicted value: "+str(inferenceString) +
          ", Actual value:"+str(test_labels[index[i]]))
