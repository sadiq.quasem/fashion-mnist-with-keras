## Fashion MNIST with Keras

This is a personal project to explore the Keras backend use with Tensorflow. For the dataset I picked Fashion MNIST, which was created to be a drop in replacement for MNIST. The model consists of several layers and includes a testing script which randomly picks 5 images from the test set to run predictions on.

To toggle training settings, the model.fit function and change parameters:
```
model.fit(train_images,
          train_labels,
          batch_size=1000,
          epochs=1,
          verbose=1,
          validation_data=(test_images, test_labels))
```
1. To train the model, run:
```
python train.py
```
2. Run the script for predictions(5 random images from training set):
```
python test.py
```
3. Your results will show
```
Predicted value: 0, Actual value:0
Predicted value: 3, Actual value:3
Predicted value: 7, Actual value:7
Predicted value: 9, Actual value:7
Predicted value: 2, Actual value:4
```

Please note: that this is a work in progress
