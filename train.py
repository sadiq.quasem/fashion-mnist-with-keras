import tensorflow as tf
from tensorflow import keras
import numpy as np

"""
Sequential Neural Network for Fashion MNIST dataset
12 layers

"""
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images,
                               test_labels) = fashion_mnist.load_data()

train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)

train_images = train_images / 255.0
test_images = test_images / 255.0

img_rows, img_cols = 28, 28

train_images = train_images.reshape(
    train_images.shape[0], img_rows, img_cols, 1)
test_images = test_images.reshape(test_images.shape[0], img_rows, img_cols, 1)
tf.keras.initializers.glorot_uniform(seed=1)

model = tf.keras.Sequential([
    # tf.keras.layers.InputLayer(input_shape=(28, 28, 1)),
    tf.keras.layers.BatchNormalization(),
    tf.keras.layers.Conv2D(64, (4, 4), padding='same', activation='relu'),
    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
    tf.keras.layers.Dropout(0.1),
    tf.keras.layers.Conv2D(64, (4, 4), activation='relu'),
    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
    tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.BatchNormalization(),
    tf.keras.layers.Dense(10, activation='softmax')
])

model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(train_images,
          train_labels,
          batch_size=1000,
          epochs=1,
          verbose=1,
          validation_data=(test_images, test_labels))

train_loss, train_acc = model.evaluate(train_images, train_labels)
print('Train accuracy', train_acc)

test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy:', test_acc)


model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("model.h5")
